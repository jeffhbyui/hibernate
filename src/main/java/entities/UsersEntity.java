package entities;

import javax.persistence.*;

@Entity
@Table(name = "users", schema = "CIT360")
public class UsersEntity {
    private Integer id;
    private String username;
    private String favcolor;
    private Long primaryid;

    @Basic
    @Column(name = "ID")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "USERNAME")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "FAVCOLOR")
    public String getFavcolor() {
        return favcolor;
    }

    public void setFavcolor(String favcolor) {
        this.favcolor = favcolor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersEntity that = (UsersEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (favcolor != null ? !favcolor.equals(that.favcolor) : that.favcolor != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (favcolor != null ? favcolor.hashCode() : 0);
        return result;
    }

    public void setPrimaryid(Long primaryid) {
        this.primaryid = primaryid;
    }

    @Id
    public Long getPrimaryid() {
        return primaryid;
    }
}
